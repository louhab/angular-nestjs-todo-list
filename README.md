# To do List 

This project is a web application built with Angular for the frontend and NestJS for the backend.

## Getting Started

### Prerequisites

Make sure you have the following installed on your machine:

- Node.js: [https://nodejs.org/](https://nodejs.org/)
- Angular CLI: Install globally using `npm install -g @angular/cli`
